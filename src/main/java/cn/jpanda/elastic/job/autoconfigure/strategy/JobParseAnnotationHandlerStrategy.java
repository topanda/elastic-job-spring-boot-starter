package cn.jpanda.elastic.job.autoconfigure.strategy;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.type.AnnotationMetadata;

public interface JobParseAnnotationHandlerStrategy {

    BeanDefinition getJobScheduler(ScannedGenericBeanDefinition beanDefinition, AnnotationMetadata metadata);
}
