package cn.jpanda.elastic.job.autoconfigure.properties;

import cn.jpanda.elastic.job.TestStarter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest(classes = TestStarter.class)
@RunWith(SpringRunner.class)
public class ElasticJobPropertiesTest {
    @Resource
    ZookeeperProperties zookeeperProperties;

    @Test
    public void start() {
        assert null != zookeeperProperties;
    }

}